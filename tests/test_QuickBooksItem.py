from quickbooks.QuickBooksItem import QuickBooksItem
import unittest

class test_QuickBooksItem(unittest.TestCase):
    def test_build(self):
        qb = QuickBooksItem() \
        .setName('New Item') \
        .setIncomeAccountRef(0, 'name') \
        .setExpenseAccountRef(1, 'name') \
        .setAssetAccountRef(2, 'name') \
        .setInvStartDate(dateStr='2017-01-15') \
        .setQtyOnHand(200) \
        .setType(QuickBooksItem.TYPE.INVENTORY)

        self.assertEqual(qb.InvStartDate, '2017-01-15')
        self.assertEqual(qb.Type, QuickBooksItem.TYPE.INVENTORY)

        qb.setInvStartDate(year=2018, month=2, day=16)

        self.assertEqual(qb.InvStartDate, '2018-02-16')
