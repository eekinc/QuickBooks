import json
from quickbooks.QuickBooksBatchOperation import QuickBooksBatchOperation
from quickbooks.QuickBooksAPIError import QuickBooksAPIError
from quickbooks.QuickBooksCustomer import QuickBooksCustomer
from quickbooks.QuickBooksInvoice import QuickBooksInvoice
from quickbooks.QuickBooksItem import QuickBooksItem
from quickbooks.quickbooks import QuickBooks
from quickbooks.QuickBooksSalesItemLine import QuickBooksSalesItemLine
import subprocess
import sys
from time import sleep, time
import unittest
import webbrowser

credentials = None
accessToken = None
realmId = None
refreshToken = None

def currentMillis():
    return int(round(time() * 1000))

def writeCredentials():
    f = open('secret.json', 'w')
    f.write(json.dumps(credentials))
    f.close()

def loadCredentials():
    global credentials

    try:
        f = open('secret.json', 'r')
        contents = f.read()
        f.close()
        credentials = json.loads(contents)
    except:
        print('Please create a credentials file: secret.json under QuickBooks/.')
        sys.exit()

loadCredentials()
qb = QuickBooks(credentials['clientId'], credentials['clientSecret'],
            credentials['redirectURI'], sandbox=True)

def getUniqueItem():
    name = "New Item " + str(currentMillis())
    item = QuickBooksItem() \
        .setName(name) \
        .setIncomeAccountRef(79, 'Sales of Product Income') \
        .setExpenseAccountRef(80, 'Cost of Goods Sold') \
        .setAssetAccountRef(81, 'Inventory Asset')

    return item

class test_QuickBooks(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        global accessToken, realmId, refreshToken

        try:
            loadCredentials()
            del credentials['code']
            del credentials['realmId']
            writeCredentials()
        except:
            pass

        uri = qb.buildOauth2ConnectURI()
        webbrowser.open(uri)

        while True:
            loadCredentials()

            if 'code' in credentials:
                break

            sleep(1)

        authCode = credentials['code'][0]
        realmId = credentials['realmId'][0]

        tokenResponse = qb.getAccessToken(authCode)
        accessToken = tokenResponse['access_token']
        refreshToken = tokenResponse['refresh_token'] 

        del credentials['code']
        del credentials['realmId']
        writeCredentials()

    def setUp(self):
        global qb, credentials

        qb = QuickBooks(credentials['clientId'], credentials['clientSecret'],
            credentials['redirectURI'], sandbox=True)

    def test_getDiscoveryDocument(self):
        doc = qb.getDiscoveryDocument()
        
        self.assertTrue('issuer' in doc)
        self.assertTrue('token_endpoint' in doc)

    def test_listAccounts(self):
        global accessToken, realmId

        qb.listAccounts(accessToken, realmId)

    def test_refreshAccessToken(self):
        global accessToken, refreshToken

        response = qb.refreshAccessToken(refreshToken)

        accessToken = response['access_token']
        refreshToken = response['refresh_token']

    def test_writeItem(self):
        name = 'New Item ' + str(currentMillis())
        item = QuickBooksItem() \
            .setName(name) \
            .setIncomeAccountRef(79, 'Sales of Product Income') \
            .setExpenseAccountRef(80, 'Cost of Goods Sold') \
            .setAssetAccountRef(81, 'Inventory Asset') \
            .setInvStartDate(dateStr='2017-01-15') \
            .setTrackQtyOnHand(True) \
            .setQtyOnHand(15.9) \
            .setType(QuickBooksItem.TYPE.INVENTORY)

        response = qb.writeItem(item, accessToken, realmId)

        self.assertEqual(float(response['Item']['QtyOnHand']), 15.9)

        syncToken = response['Item']['SyncToken']

        newName = 'Renamed item ' + str(currentMillis())
        item.setSyncToken(syncToken) \
            .setName(newName)

        response = qb.writeItem(item, accessToken, realmId)

        self.assertEqual(response['Item']['Name'], newName)

    def test_writeInvoice(self):
        name = 'New Customer ' + str(currentMillis())
        customer = QuickBooksCustomer() \
            .setDisplayName(name)

        customer = qb.writeCustomer(customer, accessToken, realmId)

        lineItem = QuickBooksSalesItemLine() \
            .setAmount(100.12345) \
            .setUnitPrice(3.50)

        invoice = QuickBooksInvoice() \
            .setCustomerRef(customer['Id'], customer['DisplayName']) \
            .addLine(lineItem)

        import json
        from quickbooks.DictEncoder import DictEncoder
        print(json.dumps(invoice, cls=DictEncoder))

        response = qb.writeInvoice(invoice, accessToken, realmId)

    def test_getItem_writeItem(self):
        name = "New Item /-'#()" + str(currentMillis())
        item = QuickBooksItem() \
            .setName(name) \
            .setIncomeAccountRef(79, 'Sales of Product Income') \
            .setExpenseAccountRef(80, 'Cost of Goods Sold') \
            .setAssetAccountRef(81, 'Inventory Asset')

        response = qb.writeItem(item, accessToken, realmId)

        itemDict = qb.getItem(name, accessToken, realmId)
        
        self.assertTrue(item is not None)
        self.assertEqual(itemDict['Name'], item.Name)

        item.setName(item.Name + '_updated')
        item.setId(itemDict['Id'])
        item.setSyncToken(itemDict['SyncToken'])
        qb.writeItem(item, accessToken, realmId)

    def test_writeCustomer_getCustomer(self):
        name = 'New Customer\'s ' + str(currentMillis())
        name = 'New Customer, ' + str(currentMillis())
        customer = QuickBooksCustomer() \
            .setDisplayName(name)

        response = qb.writeCustomer(customer, accessToken, realmId)

        self.assertEqual(response['DisplayName'], customer.DisplayName)

        response = qb.getCustomer(name, accessToken, realmId)

        self.assertEqual(response['DisplayName'], customer.DisplayName)

    def test_listCustomers(self):
        result = qb.listCustomers(accessToken, realmId)

    def test_commitBatchOperation(self):
        operation = QuickBooksBatchOperation();
        for i in range(8):
            operation.add(getUniqueItem(), 'create')

        result = qb.commitBatchOperation(operation, accessToken, realmId)

if __name__ == '__main__':
    unittest.main()