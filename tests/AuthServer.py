#!/usr/bin/python3
from http.server import BaseHTTPRequestHandler, HTTPServer
import json
from urllib.parse import parse_qs, urlparse

class authHandler(BaseHTTPRequestHandler):
  def do_GET(self):
        if self.path.startswith('/oauth2'):
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()
            self.wfile.write(bytes('You may now close this page.', 'utf8'))
            
            parsed = parse_qs(urlparse(self.path).query)

            f = open('secret.json', 'r')
            contents = f.read()
            f.close()
            credentials = json.loads(contents)

            for key in parsed:
                credentials[key] = parsed[key]

            f = open('secret.json', 'w')
            f.write(json.dumps(credentials))
            f.close()

def runAuthServer():
  server_address = ('', 5000)
  httpd = HTTPServer(server_address, authHandler)
  httpd.handle_request() # Handle only one request.

if __name__ == '__main__':
    runAuthServer()