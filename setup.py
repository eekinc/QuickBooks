#!/usr/bin/env python3

from setuptools import setup, find_packages
from setuptools.command.install import install
from subprocess import check_call as execute

repoDependencies = [
    'git+https://gitlab.com/jdsutton/py3typing'
]

class PostInstallCommand(install):
    def run(self):
        for repo in repoDependencies:
            execute('pip3 install {}'.format(repo).split())
        install.run(self)

setup(name='QuickBooks',
    version='1.0',
    description='',
    author='Eek, Inc.',
    author_email='',
    url='',
    packages=find_packages(),
    install_requires=[
        'requests'
    ],
    cmdclass={
        'install': PostInstallCommand
    }
)