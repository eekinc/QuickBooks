# QuickBooks
A Python 3 module for interacting with the Intuit QuickBooks API using OAuth2.

## Setup
1. Create a file `secret.json` under `QuickBooks/` with the following fields:
  1. `clientId` - Your app's client ID.
  2. `clientSecret` - Your app's client secret.
  3. `redirectURI` - One of your app's development redirect URIs.
  
## Running the tests
```
$ pip3 install nose
$ make test
```