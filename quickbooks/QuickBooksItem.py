from datetime import date, datetime

class QuickBooksItem:
    '''
    Represents a QuickBooks Item as specified by: https://developer.intuit.com/docs/api/accounting/item.
    '''
    PRODUCTION_BASE_URL = 'https://quickbooks.api.intuit.com'
    SANDBOX_BASE_URL = 'https://sandbox-quickbooks.api.intuit.com'
    CREATE_ENDPOINT = '/v3/company/{}/item'
    QUERY_ENDPOINT = '/v3/company/{}/query?query={}'

    class TYPE:
        INVENTORY = 'Inventory'
        SERVICE = 'Service'
        NonInventory = 'NonInventory'

    def __init__(self):
        # Name of the item, 100 chars max.
        self.Name = None
        # Item description. 4000 chars max.
        self.Description = None
        # Reference to the posting account, that is, the account that records the proceeds from the sale of this item.
        # Required for Inventory and Service item types.
        self.IncomeAccountRef = {'value': None, 'name': None}
        # Reference to the expense account used to pay the vendor for this item.
        # Required for Inventory, NonInventory, and Service item types
        self.ExpenseAccountRef = {'value': None, 'name': None}
        # Reference to the Inventory Asset account that tracks the current value of the inventory.
        # Required for Inventory item types.
        self.AssetAccountRef = {'value': None, 'name': None}
        # True if there is quantity on hand to be tracked.
        # Applicable for items of type Inventory.
        self.TrackQtyOnHand = False
        # Date of opening balance for the inventory transaction.
        # Required for Inventory type items.
        self.InvStartDate = None
        # Current quantity of the Inventory items available for sale.
        # Required for Inventory type items.
        self.QtyOnHand = None
        # Classification that specifies the use of this item.
        # Required when minor version 4 is specified.
        self.Type = None
        # Version number of the entity. 
        # Required for the update operation.
        self.SyncToken = None
        # Id of the item.
        # Required for update operation.
        self.Id = None

    @staticmethod
    def fromMetrcItem(metrcItem):
        result = QuickBooksItem() \
        .setName(metrcItem['ProductName'])

        return result

    def setName(self, value):
        self.Name = str(value)[:100]

        return self


    def setDescription(self, value):
        self.Description = str(value)[:4000]

        return self

    def setIncomeAccountRef(self, value, name):
        self.IncomeAccountRef['value'] = str(value)
        self.IncomeAccountRef['name'] = name

        return self

    def setExpenseAccountRef(self, value, name):
        self.ExpenseAccountRef['value'] = str(value)
        self.ExpenseAccountRef['name'] = name

        return self

    def setAssetAccountRef(self, value, name):
        self.AssetAccountRef['value'] = str(value)
        self.AssetAccountRef['name'] = name

        return self

    def setTrackQtyOnHand(self, value):
        self.TrackQtyOnHand = value

        return self

    def setInvStartDate(self, *, dateStr=None, year=None, month=None, day=None):
        if dateStr is not None:
            d = datetime.strptime(dateStr, '%Y-%m-%d')
        else:
            d = date(year, month, day)
        self.InvStartDate = d.strftime('%Y-%m-%d')

        return self

    def setQtyOnHand(self, value):
        self.QtyOnHand = float(value)

        return self

    def setType(self, value):
        assert(value in QuickBooksItem.TYPE.__dict__.values())
        self.Type = value

        return self

    def setSyncToken(self, value):
        self.SyncToken = value

        return self

    def setId(self, value):
        self.Id = value

        return self
