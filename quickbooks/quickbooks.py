'''
Partially based on: https://github.com/IntuitDeveloper/OAuth2PythonSampleApp/blob/master/sampleAppOAuth2/services.py
'''

import requests
from base64 import b64encode
import json
from quickbooks.DictEncoder import DictEncoder
from quickbooks.QuickBooksBatchOperation import QuickBooksBatchOperation
from quickbooks.QuickBooksAccount import QuickBooksAccount
from quickbooks.QuickBooksAPIError import QuickBooksAPIError
from quickbooks.QuickBooksAuthenticationError import QuickBooksAuthenticationError
from quickbooks.QuickBooksCustomer import QuickBooksCustomer
from quickbooks.QuickBooksInvoice import QuickBooksInvoice
from quickbooks.QuickBooksItem import QuickBooksItem
from urllib.parse import urlencode
from urllib.request import Request, urlopen

def stringToBase64(s):
    return b64encode(bytes(s, 'utf-8')).decode()

class QuickBooks:
    '''
    Class containing backend methods for interacting with the QuickBooks API.
    '''

    PRODUCTION_DISCOVERY_DOCUMENT_URL = 'https://developer.intuit.com/.well-known/openid_configuration/'
    SANDBOX_DISCOVERY_DOCUMENT_URI = 'https://developer.intuit.com/.well-known/openid_sandbox_configuration/'
    mock = False

    class SCOPE:
        ACCOUNTING = 'com.intuit.quickbooks.accounting'

    def __init__(self, clientId, clientSecret, redirectURI, *, sandbox=False):
        self._clientId = str(clientId)
        self._clientSecret = str(clientSecret)
        self._redirectURI = redirectURI
        self._sandbox = sandbox
        self._discoveryDocument = None

    @staticmethod
    def _handleResponse(response):
        if 'authentication' in str(response).lower():
            raise QuickBooksAuthenticationError(json.dumps(response))
        if response.status_code != 200:
            raise QuickBooksAPIError(json.dumps(response))

        return json.loads(response.text)

    @staticmethod
    def _escapeString(s):
        return s.replace("'", "\\'").replace('-', '\-').replace('/', '\/').replace('#', '%23')

    def getDiscoveryDocument(self):
        '''
        Fetches the discovery document.
        https://developer.intuit.com/docs/0100_quickbooks_online/0100_essentials/000500_authentication_and_authorization/connect_from_within_your_app#/Discovery_document
        '''
        if self._discoveryDocument is not None:
            return self._discoveryDocument

        if self._sandbox:
            uri = QuickBooks.SANDBOX_DISCOVERY_DOCUMENT_URI
        else:
            uri = QuickBooks.PRODUCTION_DISCOVERY_DOCUMENT_URL

        result = json.loads(requests.get(url=uri).text)

        self._discoveryDocument = result

        return result

    def buildOauth2ConnectURI(self, *, scope=None):
        if scope is None:
            scope = QuickBooks.SCOPE.ACCOUNTING

        endpoint = self.getDiscoveryDocument()['authorization_endpoint']

        data = {
            'client_id': self._clientId,
            'scope': scope,
            'redirect_uri': self._redirectURI,
            'response_type': 'code',
            'state': 'nostate'
        }

        return endpoint + '?' + urlencode(data)

    def getAccessToken(self, authCode):
        '''
        Exchanges an auth code for an access token.
        '''
        authHeader = 'Basic ' + stringToBase64(self._clientId + ':' + self._clientSecret)
        endpoint = self.getDiscoveryDocument()['token_endpoint']

        data = urlencode({
            'code': authCode,
            'redirect_uri': self._redirectURI,
            'grant_type': 'authorization_code'
        }).encode()

        headers = {
            'Authorization': authHeader,
            'content-type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
        }

        response = requests.post(endpoint, data=data, headers=headers)

        return QuickBooks._handleResponse(response)

    def refreshAccessToken(self, refreshToken):
        authHeader = 'Basic ' + stringToBase64(self._clientId + ':' + self._clientSecret)
        endpoint = self.getDiscoveryDocument()['token_endpoint']

        data = urlencode({
            'grant_type': 'refresh_token',
            'refresh_token': refreshToken
        }).encode()

        headers = {
            'Authorization': authHeader,
            'content-type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
        }

        response = requests.post(endpoint, data=data, headers=headers)

        return QuickBooks._handleResponse(response)

    def writeItem(self, item, accessToken, realmId):
        if self._sandbox:
            endpoint = QuickBooksItem.SANDBOX_BASE_URL
        else:
            endpoint = QuickBooksItem.PRODUCTION_BASE_URL
        endpoint = endpoint + QuickBooksItem.CREATE_ENDPOINT.format(realmId)

        data = json.dumps(item.__dict__).encode('utf-8')

        headers = {
            'Authorization': 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        response = requests.post(endpoint, data=data, headers=headers)

        return QuickBooks._handleResponse(response)

    def writeInvoice(self, invoice, accessToken, realmId):
        if self._sandbox:
            endpoint = QuickBooksInvoice.SANDBOX_BASE_URL
        else:
            endpoint = QuickBooksInvoice.PRODUCTION_BASE_URL
        endpoint = endpoint + QuickBooksInvoice.CREATE_ENDPOINT.format(realmId)

        data = json.dumps(invoice.__dict__, cls=DictEncoder).encode('utf-8')

        headers = {
            'Authorization': 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        response = requests.post(endpoint, data=data, headers=headers)

        return QuickBooks._handleResponse(response)

    def _post(object, endpoint, accessToken):
        pass

    def writeCustomer(self, customer, accessToken, realmId):
        if self._sandbox:
            endpoint = QuickBooksCustomer.SANDBOX_BASE_URL
        else:
            endpoint = QuickBooksCustomer.PRODUCTION_BASE_URL
        endpoint = endpoint + QuickBooksCustomer.WRITE_ENDPOINT.format(realmId)

        data = json.dumps(customer.__dict__).encode('utf-8')

        headers = {
            'Authorization': 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        response = requests.post(endpoint, data=data, headers=headers)

        return QuickBooks._handleResponse(response)['Customer']

    def queryItem(self, *, accessToken=None, realmId=None, query=None):
        if self._sandbox:
            endpoint = QuickBooksItem.SANDBOX_BASE_URL
        else:
            endpoint = QuickBooksItem.PRODUCTION_BASE_URL
        endpoint = endpoint + QuickBooksItem.QUERY_ENDPOINT.format(realmId, query)

        headers = {
            'Authorization': 'Bearer ' + accessToken,
            'Content-Type': 'text/plain',
            'Accept': 'application/json'
        }

        response = requests.post(endpoint, headers=headers)

        return QuickBooks._handleResponse(response)

    def queryCustomer(self, accessToken, realmId, query):
        if self._sandbox:
            endpoint = QuickBooksCustomer.SANDBOX_BASE_URL
        else:
            endpoint = QuickBooksCustomer.PRODUCTION_BASE_URL
        endpoint = endpoint + QuickBooksCustomer.QUERY_ENDPOINT.format(realmId, query)

        headers = {
            'Authorization': 'Bearer ' + accessToken,
            'Content-Type': 'text/plain',
            'Accept': 'application/json'
        }

        response = requests.post(endpoint, headers=headers)

        return QuickBooks._handleResponse(response)

    def getCustomer(self, customerName, accessToken, realmId):
        customerName = QuickBooks._escapeString(customerName)
        result = self.queryCustomer(accessToken, realmId,
            'select * from Customer where DisplayName = \'{}\' maxresults 1'.format(customerName))

        if 'Customer' in result['QueryResponse']:
            return result['QueryResponse']['Customer'][0]

    def getItem(self, itemName, accessToken, realmId):
        '''
        Returns a sync token if the given item exists (by name).
        Else returns None.
        '''
        itemName = QuickBooks._escapeString(itemName)
        query = 'select * from Item where Name = \'{}\' maxresults 1'.format(itemName)
        result = self.queryItem(accessToken=accessToken, realmId=realmId, query=query)

        if 'Item' in result['QueryResponse']:
            return result['QueryResponse']['Item'][0]

    def listCustomers(self, accessToken, realmId):
        result = self.queryCustomer(accessToken, realmId,
            'select * from Customer')

        if 'Customer' in result['QueryResponse']:
            return result['QueryResponse']['Customer']

    def queryAccount(self, *, accessToken=None, realmId=None, query=None):
        if self._sandbox:
            endpoint = QuickBooksAccount.SANDBOX_BASE_URL
        else:
            endpoint = QuickBooksAccount.PRODUCTION_BASE_URL
        endpoint = endpoint + QuickBooksAccount.QUERY_ENDPOINT.format(realmId, query)

        headers = {
            'Authorization': 'Bearer ' + accessToken,
            'Content-Type': 'text/plain',
            'Accept': 'application/json'
        }

        response = requests.post(endpoint, headers=headers)

        return QuickBooks._handleResponse(response)

    def listAccounts(self, accessToken, realmId):
        return self.queryAccount(realmId=realmId, accessToken=accessToken,
            query='select * from Account')

    def commitBatchOperation(self, operation, accessToken, realmId):
        if self._sandbox:
            endpoint = QuickBooksBatchOperation.SANDBOX_BASE_URL
        else:
            endpoint = QuickBooksBatchOperation.PRODUCTION_BASE_URL
        endpoint = endpoint + QuickBooksBatchOperation.ENDPOINT.format(realmId)

        data = json.dumps(operation.get()).encode('utf-8')

        headers = {
            'Authorization': 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        response = requests.post(endpoint, data=data, headers=headers)

        return QuickBooks._handleResponse(response)