class QuickBooksSalesItemLine:
    def __init__(self):
        # Required for update.
        self.Id = None

        # Required. Decimal, max 15 digits in 10.5 format.
        self.Amount = None

        '''
        Max 4000 chars. 
        Free form text description of the line item that appears in the printed record.
        '''
        self.Description = None

        self.SalesItemLineDetail = {'UnitPrice': None, 'Qty': None}

        self.DetailType = 'SalesItemLineDetail'

    def setAmount(self, value):
        self.Amount = round(value, 5)

        return self

    def getAmount(self):
        return self.Amount

    def setDescription(self, value):
        self.Description = value[:4000]

        return self

    def getDescription(self):
        return self.Description

    def setUnitPrice(self, value):
        self.SalesItemLineDetail['UnitPrice'] = value

        return self

    def getUnitPrice(self):
        return self.SalesItemLineDetail['UnitPrice']

    def setDetailQuantity(self, value):
        self.SalesItemLineDetail['Qty'] = value

        return self
