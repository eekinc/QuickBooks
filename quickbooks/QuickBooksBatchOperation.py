from quickbooks.QuickBooksItem import QuickBooksItem
from quickbooks.QuickBooksInvoice import QuickBooksInvoice
from quickbooks.QuickBooksCustomer import QuickBooksCustomer

class QuickBooksBatchOperation:
    SANDBOX_BASE_URL = 'https://sandbox-quickbooks.api.intuit.com'
    PRODUCTION_BASE_URL = 'https://quickbooks.api.intuit.com'
    ENDPOINT = '/v3/company/{}/batch'
    MAX_OPERATION_SIZE = 30

    def __init__(self):
        self._requests = []

    class UnrecognizedObjectTypeException(Exception):
        def __init__(self, item):
            super().__init__('Object type not recognized: ' + str(type(item)) + ' for object: ' \
                + str(item))

    def add(self, obj, operation):
        newObj = {}
        newObj['bId'] = 'bId' + str(len(self._requests) + 1)
        newObj['operation'] = operation
        if isinstance(obj, QuickBooksItem):
            newObj['Item'] = obj.__dict__
        elif isinstance(obj, QuickBooksInvoice):
            newObj['Invoice'] = obj.__dict__
        elif isinstance(obj, QuickBooksCustomer):
            newObj['Customer'] = obj.__dict__
        else:
            raise QuickBooksBatchOperation.UnrecognizedObjectTypeException(obj)

        self._requests.append(newObj)

        return newObj['bId']

    def get(self):
        return {'BatchItemRequest': self._requests}

    def isFull(self):
        return len(self._requests) >= QuickBooksBatchOperation.MAX_OPERATION_SIZE

    def isEmpty(self):
        return len(self._requests) == 0