class QuickBooksAccount:
    SANDBOX_BASE_URL = 'https://sandbox-quickbooks.api.intuit.com'
    PRODUCTION_BASE_URL = 'https://quickbooks.api.intuit.com'
    QUERY_ENDPOINT = '/v3/company/{}/query?query={}'

    def __init__(self, value, name):
        self.id = value
        self.name = name