class QuickBooksCustomer:
    SANDBOX_BASE_URL = 'https://sandbox-quickbooks.api.intuit.com'
    PRODUCTION_BASE_URL = 'https://quickbooks.api.intuit.com'
    WRITE_ENDPOINT = '/v3/company/{}/customer'
    QUERY_ENDPOINT = '/v3/company/{}/query?query={}'

    def __init__(self):
        # Unique identifier for this object. 
        # Required for update.
        self.Id = None
        # Version number of the object.
        # Required for update.
        self.SyncToken = None
        # The name of the person or organization as displayed.
        # Maximum 100 characters.
        self.DisplayName = None

    def setId(self, value):
        self.Id = value

        return self

    def setSyncToken(self, value):
        self.SyncToken = value

        return self

    def setDisplayName(self, value):
        self.DisplayName = value[:100]

        return self