class QuickBooksInvoice:
    SANDBOX_BASE_URL = 'https://sandbox-quickbooks.api.intuit.com'
    PRODUCTION_BASE_URL = 'https://quickbooks.api.intuit.com'
    CREATE_ENDPOINT = '/v3/company/{}/invoice'

    def __init__(self):
        self.Id = None

        # Required for update.
        self.SyncToken = None

        '''
        Required.
        Reference to a customer or job.
        Query the Customer name list resource to determine the appropriate Customer object
        for this reference. Use Customer.Id and Customer.DisplayName from that object for
        CustomerRef.value and CustomerRef.name, respectively.
        '''
        self.CustomerRef = {'value': None, 'name': None}

        # Required. Individual line items of a transaction.
        self.Line = []

    def setId(self, value):
        self.Id = value

        return self

    def getId(self):
        return self.Id

    def setSyncToken(self, value):
        self.SyncToken = value

        return self

    def getSyncToken(self):
        return self.SyncToken

    def setCustomerRef(self, value, name):
        self.CustomerRef['value'] = value
        self.CustomerRef['name'] = name

        return self

    def getCustomerRef(self):
        return self.CustomerRef

    def addLine(self, line):
        self.Line.append(line)

        return self

    def getLine(self):
        return self.Line
